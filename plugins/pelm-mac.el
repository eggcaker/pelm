;;; pelm-mac.el --- PELM configuration for mac
;;
;; Copyright (c) 2011-2015 eggcaker
;;
;; Authors: eggcaker <eggcaker@gmail.com>
;; URL: http://caker.me/pelm

;; This file is not part of GNU Emacs

;;; Code:


;; under mac, have Command as Meta and keep Option for localized input
(when (string-match "apple-darwin" system-configuration)

  (set-face-font 'default "Monaco-18")
 ; (set-face-font 'default "Menlo-16")
;  (set-face-font 'default "Anonymous_Pro-18")
;  (set-face-font 'default "BPtypewrite-18")
  (setq mac-allow-anti-aliasing t)
  (setq mac-command-modifier 'meta)
  (setq mac-option-modifier 'none)
  (define-key pelm-keymap "uf" 'ns-toggle-fullscreen))


(provide 'pelm-mac)
;;; ends pelm-mac.el here



