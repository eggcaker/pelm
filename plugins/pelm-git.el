;;; pelm-git.el --- PELM  git
;;
;; Copyright (c) 2011-2015 eggcaker
;;
;; Authors: eggcaker <eggcaker@gmail.com>
;; URL: http://caker.me/pelm

;; This file is not part of GNU Emacs

;;; Code:



(provide 'pelm-git)
;;perlm-git.el ends here
